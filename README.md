####Kompilacja i uruchamianie:
Poniższa komenda skompiluje, uruchomi testy oraz ostatecznie aplikację

`sbt clean test run`

#### Opis działania
Do aplikacji dodany jest prosty interfejs HTTP (RESTful web service).
Aplikacja nie traci dane po wyłączeniu.

* dodanie taska: `PUT /task`

```
{
  "id": 1,
  "question": "...",
  "points": 1.5,
  "options": ["...", "..."],
  "answer": [0]
}
```

* usunięcie taska: `DELETE /task/1`
* pobranie taska: `GET /task/1`
* wylosowanie taska: `GET /task/random?user=1`

* dodanie użytkownika: `PUT /user/name`
* usunięcie użytkownika: `DELETE /user/name`
* odpowiedź na pytanie: `POST /user/name`
```
{
    "taskId": 1,
    "options": [0, 1]
}
```
* ranking: `GET /ranking`