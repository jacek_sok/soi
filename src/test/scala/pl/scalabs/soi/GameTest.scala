package pl.scalabs.soi

import akka.actor.ActorRef
import akka.pattern.ask
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually._
import pl.scalabs.soi.domain.{Ranking, TaskManager, UserManager}

class GameTest extends BaseSpec with AkkaSupport {
  implicit val config = ConfigFactory.load()

  trait TestCtx {
    val taskManager: ActorRef = TaskManager.create()
    val ranking: ActorRef = Ranking.create()
    val userManager: ActorRef = UserManager.create(taskManager, ranking)
  }

  "Game" should "create correct ranking based on users answers" in new TestCtx {
    waitFor(
      taskManager ? AddTask(TaskDefinition(1, 10, "Question1", List("Opt1", "Opt2"), Set(0))),
      taskManager ? AddTask(TaskDefinition(2, 20, "Question1", List("Opt1", "Opt2", "Opt2"), Set(1, 2))),
      taskManager ? AddTask(TaskDefinition(3, 5, "Question1", List("Opt1", "Opt2", "Opt3", "Opt4"), Set(0, 1, 2))),
      userManager ? CreateUser("user-1"),
      userManager ? CreateUser("user-2")
    )

    eventually {
      waitForResult[RankingList](ranking ? GetRanking) should contain only
        Seq(
          UserScore("user-1", 0.0),
          UserScore("user-2", 0.0)
        )
    }

    val user1 = waitForResult[ActorRef](userManager ? GetUser("user-1"))
    val user2 = waitForResult[ActorRef](userManager ? GetUser("user-2"))

    // synchronise so we know how the ranking list should look
    waitFor(user1 ? PostAnswer(1, Set(0)))
    waitFor(user2 ? PostAnswer(1, Set(0)))

    user1 ! PostAnswer(1, Set(0)) // try to answer the same task
    user1 ! PostAnswer(2, Set(1)) // incorrect answer

    user2 ! PostAnswer(2, Set(1, 2))

    eventually {
      waitForResult[RankingList](ranking ? GetRanking) should contain inOrderOnly(
        Seq(UserScore("user-2", 29.5)),
        Seq(UserScore("user-1", 10.0))
      )
    }
  }
  it should "create correct ranking based for concurrent answers to the same task" in new TestCtx{
    val noOfUsers = 500
    val points = 14.0

    waitFor(taskManager ? AddTask(TaskDefinition(1, points, "Question1", List("Opt1", "Opt2"), Set(0, 1))))
    val users = for (i <- 0 until noOfUsers)
      yield waitForResult[ActorRef](userManager ? CreateUser(s"user-$i"))

    for (user <- users) yield user ! PostAnswer(1, Set(0, 1))

    eventually {
      val list = waitForResult[RankingList](ranking ? GetRanking)
      list should have size noOfUsers
      list.flatten.map(_.points).distinct.size shouldBe list.flatten.size
    }
  }
}
