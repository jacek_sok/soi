package pl.scalabs.soi

import akka.actor.ActorSystem
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Args, Status, SuiteMixin}

import scala.concurrent.ExecutionContext

trait AkkaSupport extends SuiteMixin with ScalaFutures {
  this: BaseSpec =>

  implicit var actorSystem: ActorSystem = _
  implicit var executionContext: ExecutionContext = _

  abstract override protected def runTest(testName: String, args: Args): Status = {
    actorSystem = ActorSystem("TestSystem")
    executionContext = actorSystem.dispatcher
    try super.runTest(testName, args)
    finally waitFor(actorSystem.terminate())
  }
}
