package pl.scalabs.soi

import akka.util.Timeout
import org.scalatest.concurrent.Eventually
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.reflect.ClassTag

trait BaseSpec extends FlatSpec with Matchers {
  implicit val askTimeout = Timeout(5 seconds)
  implicit val eventuallyPatience = Eventually.PatienceConfig(5 seconds, 100 millis)

  def waitForResult[T](f: Future[Any])(implicit classTag: ClassTag[T], executionContext: ExecutionContext): T =
    Await.result(f.mapTo[T], 5 seconds)

  def waitFor(f: Future[Any]*)(implicit executionContext: ExecutionContext): Unit = {
    val fixedFuture = f match {
      case head :: Nil => head
      case _ => Future.sequence(f)
    }
    Await.result(fixedFuture, 5 seconds)
  }
}
