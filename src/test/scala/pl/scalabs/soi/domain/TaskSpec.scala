package pl.scalabs.soi.domain

import akka.pattern.ask
import org.scalatest.prop.Checkers
import pl.scalabs.soi._

import scala.concurrent.Future

class TaskSpec extends BaseSpec with Checkers with AkkaSupport {
  val SampleTask = TaskDefinition(
    id = 1,
    points = 10,
    question = "?",
    options = List("A", "B", "C", "D"),
    answer = Set(1, 3)
  )

  "A Task" should "accept correct answer replying with score and notifying ranking" in {
    val task = Task.create(SampleTask)
    
    val response = (task ? TryAnswer(Set(1, 3))).mapTo[CorrectAnswer]

    whenReady(response) { rs =>
      rs shouldBe CorrectAnswer(1, 10)
    }
  }
  it should "reject incorrect answer replying with correct answer" in {
    val task = Task.create(SampleTask)

    val response = (task ? TryAnswer(Set(1))).mapTo[IncorrectAnswer]

    whenReady(response) { rs =>
      rs shouldBe IncorrectAnswer(1, Set(1), Set(1, 3))
    }
  }
  it should "correctly calculate following scores" in {
    val task = Task.create(SampleTask)

    val responses = for (_ <- 0 until 5)
      yield (task ? TryAnswer(Set(1, 3))).mapTo[CorrectAnswer]

    whenReady(Future.sequence(responses)) { rs =>
      rs should contain inOrderOnly(
        CorrectAnswer(1, 10),
        CorrectAnswer(1, 9.5),
        CorrectAnswer(1, 9.025),
        CorrectAnswer(1, 8.57375),
        CorrectAnswer(1, 8.1450625)
      )
    }
  }
  it should "not set same score for any correct answer" in {
    val task = Task.create(SampleTask)

    def simulate(n: Int) = Future {
      val responses = for (_ <- 0 until 100)
        yield (task ? TryAnswer(Set(1, 3))).mapTo[CorrectAnswer]
      waitForResult[Seq[CorrectAnswer]](Future.sequence(responses))
    }

    val allResponses = Future.sequence(
      List(simulate(1), simulate(2), simulate(3), simulate(4))
    ).map(_.flatten)

    whenReady(allResponses) { rs =>
      val points = rs.map(_.points)
      assert(points.size == points.distinct.size, "Task replied with the same score more than once")
    }
  }
}
