package pl.scalabs.soi.domain

import akka.pattern.ask
import org.scalatest.concurrent.Eventually.eventually
import pl.scalabs.soi._

class TaskManagerSpec extends BaseSpec with AkkaSupport {
  val SampleTask1 = TaskDefinition(1, 1, "?", List.empty, Set.empty)
  val SampleTask2 = TaskDefinition(2, 2, "?", List.empty, Set.empty)

  "Task Manager" should "add task" in {
    val manager = TaskManager.create()

    whenReady(manager ? AddTask(SampleTask1)) {
      _ shouldBe TaskAccepted
    }
  }
  it should "reject task due to ID duplication" in {
    val manager = TaskManager.create()

    manager ! AddTask(SampleTask1)

    whenReady(manager ? AddTask(SampleTask1)) {
      _ shouldBe TaskRejected("Duplicated #1")
    }
  }
  it should "remove task" in {
    val manager = TaskManager.create()

    manager ! AddTask(SampleTask1)
    manager ! AddTask(SampleTask2)

    eventually {
      whenReady((manager ? GetTasks).mapTo[Iterable[TaskDefinition]]) {
        _ should contain only (SampleTask1, SampleTask2)
      }
    }

    waitForResult[TaskResponse](manager ? RemoveTask(1)) shouldBe TaskRemoved

    eventually {
      whenReady((manager ? GetTasks).mapTo[Iterable[TaskDefinition]]) {
        _ should contain only SampleTask2
      }
    }
  }
  it should "return reference to a task" in {
    val manager = TaskManager.create()

    manager ! AddTask(SampleTask1)

    eventually {
      whenReady(manager ? GetTask(1)) {
        _ shouldBe an[TaskHolder]
      }
    }
  }
  it should "not return reference if task doesn't exists" in {
    val manager = TaskManager.create()

    eventually {
      whenReady(manager ? GetTask(1)) {
        _ shouldBe TaskNotFound
      }
    }
  }
  it should "return random task not within provided set" in {
    val manager = TaskManager.create()

    manager ! AddTask(SampleTask1)
    manager ! AddTask(SampleTask2)

    whenReady(manager ? GetTaskNotIn(Set(1))) {
      _ shouldBe Some(SampleTask2)
    }
  }
  it should "return no task if there are no available tasks to pick" in {
    val manager = TaskManager.create()

    manager ! AddTask(SampleTask1)
    manager ! AddTask(SampleTask2)

    whenReady(manager ? GetTaskNotIn(Set(1, 2))) {
      _ shouldBe None
    }
  }
  it should "return task definition with updated points" in {
    val manager = TaskManager.create()

    // add task
    manager ! AddTask(SampleTask1)
    val TaskHolder(_, actor) = waitForResult[TaskHolder](manager ? GetTask(1))
    // provide correct answer to the task - this will decrease number of points
    actor ! TryAnswer(SampleTask1.answer)

    // get updated task
    eventually {
      whenReady(manager ? GetTask(1)) {
        _ shouldBe TaskHolder(SampleTask1.copy(points = 0.95), actor)
      }
    }
  }
}
