package pl.scalabs.soi.domain

import akka.actor.{ActorRef, PoisonPill}
import akka.pattern.ask
import org.scalacheck.Prop.forAll
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.prop.Checkers
import pl.scalabs.soi._

class RankingSpec extends BaseSpec with Checkers with AkkaSupport {
  trait TestCtx {
    val ranking: ActorRef = Ranking.create()

    def rankingList(): RankingList =
      waitForResult[RankingList](ranking ? GetRanking)
  }

  "A Ranking" should "add user to the ranking" in new TestCtx {
    ranking ! UserScore("user", 1.11)

    eventually {
      rankingList() should contain only Seq(UserScore("user", 1.11))
    }
  }
  it should "remove user from ranking" in new TestCtx {
    ranking ! UserScore("user", 0.1)
    eventually {
      rankingList() should contain only Seq(UserScore("user", 0.1))
    }

    ranking ! RemoveFromRanking("user")
    eventually {
      rankingList() shouldBe empty
    }
  }
  it should "update user score" in new TestCtx {
    ranking ! UserScore("user1", 1.11)
    ranking ! UserScore("user2", 2.11)
    ranking ! UserScore("user3", 0.11)
    ranking ! UserScore("user1", 4.00)

    eventually {
      rankingList()  should contain inOrderOnly(
        Seq(UserScore("user1", 4.00)),
        Seq(UserScore("user2", 2.11)),
        Seq(UserScore("user3", 0.11))
      )
    }
  }

  it should "handle users with same scores" in new TestCtx {
    ranking ! UserScore("userB", 1.11)
    ranking ! UserScore("userA", 1.11)
    ranking ! UserScore("userC", 0.11)

    eventually {
      rankingList() should contain inOrderOnly(
        Seq(
          UserScore("userA", 1.11),
          UserScore("userB", 1.11)
        ),
        Seq(UserScore("userC", 0.11))
      )
    }
  }

  it should "always return sorted list" in {
    check {
      forAll { scores: List[Double] =>
        val ranking = Ranking.create()

        var i = 0
        for (s <- scores) {
          ranking ! UserScore(s"user_$i", s)
          i = i + 1
        }

        try whenReady((ranking ? GetRanking).mapTo[RankingList]) { rankingList =>
          rankingList === rankingList.sortBy(-_.head.points)
        } finally {
          ranking ! PoisonPill
          Thread.sleep(50)
        }
      }
    }
  }
}
