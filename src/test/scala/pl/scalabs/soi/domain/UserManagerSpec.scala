package pl.scalabs.soi.domain

import akka.actor.ActorRef
import akka.pattern.ask
import akka.testkit.TestProbe
import com.typesafe.config.ConfigFactory
import pl.scalabs.soi._

class UserManagerSpec extends BaseSpec with AkkaSupport {
  implicit private val config = ConfigFactory.load()

  trait TestCtx {
    val taskManager: TestProbe = TestProbe()
    val ranking: TestProbe = TestProbe()
    val userManager: ActorRef = UserManager.create(taskManager.testActor, ranking.testActor)
  }

  "User Manager" should "create new user" in new TestCtx {
    whenReady(userManager ? CreateUser("tstusr")) {
      _ shouldBe a[ActorRef]
    }
  }
  it should "remove user from game" in new TestCtx {
    waitFor(userManager ? CreateUser("test-user"))

    whenReady(userManager ? RemoveUser("test-user")) { rs =>
      rs shouldBe UserRemoved
      ranking.expectMsgAllOf(
        UserScore("test-user", 0.0),
        RemoveFromRanking("test-user")
      )
    }
  }
  it should "provide reference to a user" in new TestCtx {
    waitFor(userManager ? CreateUser("test-user"))

    whenReady(userManager ? GetUser("test-user")) {
      _ shouldBe a[ActorRef]
    }
  }
  it should "fail to add user if it already exists" in new TestCtx {
    waitFor(userManager ? CreateUser("test-user"))

    whenReady(userManager ? CreateUser("test-user")) {
      _ shouldBe UserAlreadyExists
    }
  }
  it should "not remove user if it does not exists" in new TestCtx {
    whenReady(userManager ? RemoveUser("test-user")) {
      _ shouldBe UserDoesNotExists
    }
  }
  it should "fail to get reference to non-existing user" in new TestCtx {
    whenReady(userManager ? GetUser("test-user")) {
      _ shouldBe UserDoesNotExists
    }
  }
}
