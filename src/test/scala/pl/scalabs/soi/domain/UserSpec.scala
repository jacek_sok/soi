package pl.scalabs.soi.domain

import akka.actor.ActorDSL._
import akka.actor.ActorRef
import akka.pattern.{AskTimeoutException, ask}
import akka.testkit.TestProbe
import org.scalatest.concurrent.Eventually.eventually
import pl.scalabs.soi._

import scala.concurrent.duration.{DurationInt, FiniteDuration}

class UserSpec extends BaseSpec with AkkaSupport {
  implicit val patience = PatienceConfig(3 seconds, 100 millis)
  val task = TaskDefinition(2, 3.11, "...", List("A", "B", "C", "D"), Set(0, 1))

  trait TestCtx {
    val taskManager: ActorRef = TaskManager.create()
    val taskTimeout: FiniteDuration = 2 seconds
    val ranking = TestProbe("Ranking")
    lazy val user: ActorRef = User.create("usr", taskTimeout, taskManager, ranking.testActor)
  }

  "An User" should "forward answer to correct task and reply with result" in new TestCtx {
    waitFor(taskManager ? AddTask(task))

    whenReady(user ? PostAnswer(2, Set(0, 1))) {
      _ shouldBe CorrectAnswer(2, 3.11)
    }
  }
  it should "not allow to answer same question more then once" in new TestCtx {
    user ! CorrectAnswer(1, 0.9)

    whenReady(user ? PostAnswer(1, Set(1))) {
      _ shouldBe TaskAlreadyTried
    }
  }
  it should "check already answered question" in new TestCtx {
    waitFor(taskManager ? AddTask(task))

    user ! PostAnswer(2, Set(0, 1))

    eventually {
      whenReady(user ? Check(2)) {
        _ shouldBe CorrectAnswer(2, 3.11)
      }
    }
  }
  it should "check not answered question" in new TestCtx {
    whenReady(user ? Check(2)) {
      _ shouldBe TaskNotTried
    }
  }
  it should "update ranking with received score" in new TestCtx {
    user ! CorrectAnswer(1, 0.9)
    user ! CorrectAnswer(2, 2.1)

    ranking.expectMsgAllOf(
      UserScore("usr", 0.0),
      UserScore("usr", 0.9),
      UserScore("usr", 3.0)
    )
  }
  it should "fail to answer to non-existing task" in new TestCtx {
    whenReady(user ? PostAnswer(1, Set(0))) {
      _ shouldBe TaskNotFound
    }
  }
  it should "handle task manager timeout" in new TestCtx {
    override val taskManager = TestProbe().testActor // dummy TaskManager actor will cause timeout

    whenReady((user ? PostAnswer(1, Set(0))).failed) {
      _ shouldBe an[AskTimeoutException]
    }
  }
  it should "handle unexpected response from task manager" in new TestCtx {
    override val taskManager = actor(new Act {
      become {
        case GetTask(_) => sender() ! "XYZ" // reply with some garbage
      }
    })

    whenReady((user ? PostAnswer(1, Set(0))).failed) {
      _ shouldBe an[RuntimeException]
    }
  }
  it should "handle task timeout" in new TestCtx {
    override val taskTimeout = 100.milliseconds
    override val taskManager = actor(new Act {
      become {
        // dummy Task actor - will cause timeout
        case GetTask(_) => sender() ! TaskHolder(task, TestProbe().testActor)
      }
    })

    whenReady(user ? PostAnswer(2, Set(0, 1))) {
      _ shouldBe TaskTimeOut
    }
  }
}
