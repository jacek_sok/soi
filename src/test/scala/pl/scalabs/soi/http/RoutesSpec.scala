package pl.scalabs.soi.http

import akka.actor.ActorDSL._
import akka.actor.{ActorRef, PoisonPill}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.pattern.ask
import akka.testkit.TestProbe
import com.typesafe.config.ConfigFactory
import pl.scalabs.soi._
import pl.scalabs.soi.domain.{Ranking, UserManager}
import org.scalatest.concurrent.Eventually.eventually

import scala.concurrent.duration.DurationInt

class RoutesSpec extends BaseSpec with ScalatestRouteTest {

  implicit val routeTimeout = RouteTestTimeout(5 seconds)

  trait TestCtx extends JsonSupport {
    implicit val config = ConfigFactory.load()
    lazy val context = Context.default()
    lazy val routes = new Routes(context).routing

    def runIsolated(code: => Any): Unit = {
      try code finally {
        context.taskManager ! PoisonPill
        context.userManager ! PoisonPill
        context.ranking ! PoisonPill
        Thread.sleep(100)
      }
    }
  }

  val task1 = TaskDefinition(1, 10, "Q1", List("O1", "O2", "O3"), Set(2))
  val task2 = TaskDefinition(2, 20, "Q2", List("O1", "O2", "O3", "O4"), Set(0, 2))
  val task3 = TaskDefinition(3, 30, "Q3", List("O1", "O2", "O3", "O4"), Set(0, 1, 2))

  "Http Service" should "create a task" in new TestCtx {
    runIsolated {
      Put("/task", task1) ~> routes ~> check {
        status === Created
      }
    }
  }
  it should "handle task duplication" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)

      Put("/task", task1) ~> routes ~> check {
        status === Conflict
      }
    }
  }
  it should "delete task" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)

      Delete("/task/1") ~> routes ~> check {
        status === Accepted
      }
    }
  }
  it should "handle deletion of non-existing task" in new TestCtx {
    runIsolated {
      Delete("/task/1") ~> routes ~> check {
        status === NotFound
      }
    }
  }
  it should "retrieve task definition" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)

      Get("/task/1") ~> routes ~> check {
        status === NotFound
        responseAs[TaskDefinition] shouldBe task1
      }
    }
  }
  it should "handle retrieval of non-existing task" in new TestCtx {
    runIsolated {
      Get("/task/1") ~> routes ~> check {
        status === NotFound
      }
    }
  }
  it should "get random task for user" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)
      context.taskManager ! AddTask(task2)
      context.taskManager ! AddTask(task3)
      context.userManager ! CreateUser("test-user")

      var answered = Set.empty[Int]
      for (_ <- 1 to 3) {
        Get("/task/random?user=test-user") ~> routes ~> check {
          status === OK
          val taskId = responseAs[TaskDetail].id
          answered should not contain taskId
          Post("/user/test-user", PostAnswer(taskId, Set(0))) ~> routes ~> check {
            status === OK
            answered = answered + taskId
          }
        }
      }
    }
  }
  it should "not return any task if user answered for all of them" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)

      waitFor {
        (context.userManager ? CreateUser("test-user")).mapTo[ActorRef] map { userActor =>
          waitFor(userActor ? PostAnswer(task1.id, task1.answer))
        }
      }

      Get("/task/random?user=test-user") ~> routes ~> check {
        status === NoContent
      }
    }
  }
  it should "create user" in new TestCtx {
    runIsolated {
      Put("/user/test-user") ~> routes ~> check {
        status === Created
      }
    }
  }
  it should "handle duplication of user" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")

      Put("/user/test-user") ~> routes ~> check {
        status === Conflict
      }
    }
  }
  it should "remove user" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")

      Delete("/user/test-user") ~> routes ~> check {
        status === Accepted
      }
    }
  }
  it should "handle removal of non-existing user" in new TestCtx {
    runIsolated {
      Delete("/user/test-user") ~> routes ~> check {
        status === NotFound
      }
    }
  }
  it should "handle correct answer" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")
      context.taskManager ! AddTask(task1)

      Post("/user/test-user", PostAnswer(1, task1.answer)) ~> routes ~> check {
        status === OK
        responseAs[CorrectAnswer] shouldBe CorrectAnswer(1, 10)
      }
    }
  }
  it should "handle incorrect answer" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")
      context.taskManager ! AddTask(task1)

      Post("/user/test-user", PostAnswer(1, Set(0, 2))) ~> routes ~> check {
        status === OK
        responseAs[IncorrectAnswer] shouldBe IncorrectAnswer(1, Set(0, 2), task1.answer)
      }
    }
  }
  it should "handle answer of non-existing user" in new TestCtx {
    runIsolated {
      context.taskManager ! AddTask(task1)

      Post("/user/test-user", PostAnswer(1, Set(0, 2))) ~> routes ~> check {
        status === NotFound
      }
    }
  }
  it should "handle answer for non-existing task" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")

      Post("/user/test-user", PostAnswer(1, Set(0, 2))) ~> routes ~> check {
        status === NotFound
      }
    }
  }
  it should "handle double answer to the same question" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("test-user")
      context.taskManager ! AddTask(task1)

      Post("/user/test-user", PostAnswer(1, Set(0, 2))) ~> routes ~> check {
        status === OK
      }
      Post("/user/test-user", PostAnswer(1, Set(0, 2))) ~> routes ~> check {
        status === Gone
      }
    }
  }
  it should "handle task timeout" in new TestCtx {
    override lazy val context: Context = {
      val ranking = Ranking.create()
      val taskManager = actor {
        new Act {
          become {
            case GetTask(1) =>
              // reply with dummy actor that will cause timeouts by not responding
              sender() ! TaskHolder(task1, TestProbe().testActor)
          }
        }
      }
      Context(ranking, taskManager, UserManager.create(taskManager, ranking), system, config)
    }

    runIsolated {
      context.userManager ! CreateUser("test-user")

      Post("/user/test-user", PostAnswer(1, task1.answer)) ~> routes ~> check {
        status === GatewayTimeout
      }
    }
  }
  it should "get ranking" in new TestCtx {
    runIsolated {
      context.userManager ! CreateUser("usr1")
      context.userManager ! CreateUser("usr2")
      context.userManager ! CreateUser("usr3")

      context.taskManager ! AddTask(task1)
      context.taskManager ! AddTask(task2)
      context.taskManager ! AddTask(task3)

      Post("/user/usr1", PostAnswer(1, task1.answer)) ~> routes
      Post("/user/usr2", PostAnswer(1, task1.answer)) ~> routes
      Post("/user/usr1", PostAnswer(2, task2.answer)) ~> routes

      eventually {
        Get("/ranking") ~> routes ~> check {
          status === OK
          responseAs[RankingList] shouldBe List(
            Seq(UserScore("usr1", 30)),
            Seq(UserScore("usr2", 9.5)),
            Seq(UserScore("usr3", 0))
          )
        }
      }
    }
  }
}
