package pl.scalabs.soi

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.Config
import pl.scalabs.soi.domain.{Ranking, TaskManager, UserManager}

import scala.concurrent.duration.{Duration, FiniteDuration}

object Context {
  def default()(implicit actorSystem: ActorSystem, config: Config): Context = {
    val ranking = Ranking.create()
    val taskManager = TaskManager.create()
    val userManager = UserManager.create(taskManager, ranking)

    Context(ranking, taskManager, userManager, actorSystem, config)
  }

  implicit class ConfigExt(config: Config) {
    def getFiniteDuration(path: String): FiniteDuration = {
      val timeout = config.getString(path)
      Duration(timeout) match {
        case fd: FiniteDuration => fd
        case d: Duration => throw new RuntimeException(s"Not a FiniteDuration: $timeout")
      }
    }
  }
}

case class Context(ranking: ActorRef,
                   taskManager: ActorRef,
                   userManager: ActorRef,
                   actorSystem: ActorSystem,
                   config: Config)
