package pl.scalabs.soi.domain

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import pl.scalabs.soi._

object Task {
  def props(taskDefinition: TaskDefinition): Props =
    Props(classOf[Task], taskDefinition)

  def create(taskDefinition: TaskDefinition)(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(props(taskDefinition), s"Task-${taskDefinition.id}")
}

class Task(taskDefinition: TaskDefinition) extends Actor with ActorLogging {
  private var points = taskDefinition.points

  log.info(s"Setting up task #${taskDefinition.id} with starting points: $points")

  private def updatePoints() = {
    points = points * 0.95
    context.parent ! PointsUpdated(taskDefinition.id, points)
    log.debug(s"Task #${taskDefinition.id} next points: $points")
  }

  override def receive: Receive = {
    case TryAnswer(answers) if taskDefinition.answer == answers =>
      sender() ! CorrectAnswer(taskDefinition.id, points)
      updatePoints()

    case TryAnswer(answers) =>
      sender() ! IncorrectAnswer(taskDefinition.id, answers, taskDefinition.answer)
  }
}
