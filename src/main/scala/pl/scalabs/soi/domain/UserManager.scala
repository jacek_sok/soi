package pl.scalabs.soi.domain

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import com.typesafe.config.Config
import pl.scalabs.soi._

object UserManager {
  def create(taskManager: ActorRef, ranking: ActorRef)
            (implicit actorSystem: ActorSystem, config: Config): ActorRef =
    actorSystem.actorOf(Props(classOf[UserManager], taskManager, ranking, config))
}

class UserManager(taskManager: ActorRef, ranking: ActorRef)(implicit config: Config) extends Actor with ActorLogging  {
  private var users = Map.empty[String, ActorRef]

  private def withUserRef(name: String)(code: ActorRef => Any) =
    users.get(name).fold {
      sender() ! UserDoesNotExists
    } { ref =>
      code(ref)
    }

  override def receive: Receive = {
    case CreateUser(name) if users.contains(name) =>
      sender() ! UserAlreadyExists

    case CreateUser(name) =>
      val ref = context.actorOf(User.props(name, taskManager, ranking))
      users = users + (name -> ref)
      sender() ! ref
      log.info(s"User created #$name")

    case RemoveUser(name) =>
      withUserRef(name) { ref =>
        users = users - name
        context.stop(ref)
        ranking ! RemoveFromRanking(name)
        sender() ! UserRemoved
        log.info(s"User removed #$name")
      }

    case GetUser(name) =>
      withUserRef(name){
        sender() ! _
      }
  }
}
