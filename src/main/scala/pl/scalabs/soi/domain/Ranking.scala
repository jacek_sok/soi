package pl.scalabs.soi.domain

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import pl.scalabs.soi._

object Ranking {
  def props(): Props =
    Props(classOf[Ranking])

  def create()(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(props(), "Ranking")

  private [Ranking] case class Wrapper(userScore: UserScore) {
    override def hashCode(): Int = userScore.user.hashCode

    override def equals(obj: scala.Any): Boolean = obj match {
      case Wrapper(UserScore(user, _)) => user == userScore.user
      case _ => false
    }
  }
}

class Ranking extends Actor with ActorLogging {
  import Ranking.Wrapper

  private var scores = Set.empty[Wrapper]
  private var sorted: Option[RankingList] = None

  override def receive: Receive = {
    case newScore: UserScore =>
      log.debug(s"Updating user score: $newScore")
      val wrapper = Wrapper(newScore)
      scores = (scores - wrapper) + wrapper
      sorted = None

    case RemoveFromRanking(user) =>
      log.info(s"Removing user: $user")
      scores = scores - Wrapper(UserScore(user, 0))
      sorted = None

    case GetRanking =>
      if (sorted.isEmpty) {
        log.debug("Building ranking list")
        sorted = Some(
          scores
            .groupBy(_.userScore.points)
            .values.map(_.map(_.userScore).toSeq.sortBy(_.user))
            .toList
            .sortBy(-_.head.points)
        )
      }
      sender() ! sorted.get

    case 'clear =>
      scores = Set.empty
  }
}
