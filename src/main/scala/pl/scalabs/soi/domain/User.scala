package pl.scalabs.soi.domain

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.Config
import pl.scalabs.soi._

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.util.{Failure, Success}

object User {
  import pl.scalabs.soi.Context.ConfigExt

  def props(name: String, taskManager: ActorRef, ranking: ActorRef)(implicit config: Config): Props =
    Props(classOf[User], name, config.getFiniteDuration("soi.task-timeout"), taskManager, ranking)

  def props(name: String, taskTimeout: FiniteDuration, taskManager: ActorRef, ranking: ActorRef): Props =
    Props(classOf[User], name, taskTimeout, taskManager, ranking)

  def create(name: String, taskTimeout: FiniteDuration, taskManager: ActorRef, ranking: ActorRef)(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(props(name, taskTimeout, taskManager, ranking), s"User_$name")

  private[User] case class TaskTimedOut(taskId: Int)
}

class User(name: String, taskTimeout: FiniteDuration, taskManager: ActorRef, ranking: ActorRef) extends Actor with ActorLogging {

  import User.TaskTimedOut

  private var answers = Map.empty[Int, Option[AnswerResult]]
  private var replies = Map.empty[Int, ActorRef]
  private var score = 0.0

  private implicit val timeout = Timeout(1 second)
  import context.dispatcher

  override def preStart(): Unit = super.preStart()
    updateAndPropagateScore(0)

  private def updateTaskStatus(taskId: Int, result: AnswerResult) =
    answers = answers + (taskId -> Some(result))

  private def updateAndPropagateScore(points: Double) = {
    score = score + points
    ranking ! UserScore(name, score)
  }

  private def reply(taskId: Int, result: Any) =
    replies.get(taskId).foreach { actorRef =>
      actorRef ! result
      replies = replies - taskId
    }

  private def scheduleTaskTimeout(taskId: Int) =
    context.system.scheduler.scheduleOnce(taskTimeout, self, TaskTimedOut(taskId))

  private def forwardAnswer(taskActor: ActorRef, replyTo: ActorRef, pa: PostAnswer) = {
    import pa._
    taskActor ! TryAnswer(options)
    answers = answers + (taskId -> None)
    replies = replies + (taskId -> replyTo)
    scheduleTaskTimeout(taskId)
  }

  private def handleFailure(replyTo: ActorRef, error: Throwable) = {
    log.error(error, "Failed to get task from manager")
    replyTo ! akka.actor.Status.Failure(error)
  }

  override def receive: Receive = {
    case PostAnswer(taskId, _) if answers.contains(taskId) =>
      sender() ! TaskAlreadyTried

    case pa@PostAnswer(taskId, options) =>
      val replyTo = sender()
      log.debug(s"Posting answer to task #$taskId (options: $options)")

      (taskManager ? GetTask(taskId)) onComplete {
        case Success(TaskHolder(_, actorRef)) =>
          forwardAnswer(actorRef, replyTo, pa)

        case Success(TaskNotFound) =>
          replyTo ! TaskNotFound

        case Success(other) =>
          handleFailure(replyTo, new RuntimeException(s"Unexpected response: $other"))

        case Failure(error) =>
          handleFailure(replyTo, error)
      }

    case result@CorrectAnswer(taskId, points) =>
      log.debug(s"User #$name, correct answer to #$taskId")
      updateTaskStatus(taskId, result)
      updateAndPropagateScore(points)
      reply(taskId, result)

    case result@IncorrectAnswer(taskId, _, _) =>
      log.debug(s"User #$name, incorrect answer to #$taskId")
      updateTaskStatus(taskId, result)
      reply(taskId, result)

    case Check(taskId) =>
      val result = answers.get(taskId)
        .fold[Any](TaskNotTried) {
          case None => TaskInProgress
          case Some(r) => r
        }

      sender() ! result

    case TaskTimedOut(taskId) =>
      log.warning(s"User #$name task timeout #$taskId")
      reply(taskId, TaskTimeOut)

    case PickRandomTask =>
      val replyTo = sender()
      (taskManager ? GetTaskNotIn(answers.keySet)) onComplete {
        case Success(result) => replyTo ! result
        case Failure(error) => handleFailure(replyTo, error)
      }
  }
}
