package pl.scalabs.soi.domain

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import pl.scalabs.soi._

object TaskManager {
  def create()(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(Props(classOf[TaskManager]), "TaskManager")
}

class TaskManager extends Actor with ActorLogging  {
  private var tasks = Map.empty[Int, TaskHolder]

  override def receive: Receive = {
    case AddTask(TaskDefinition(id, _, _, _, _)) if tasks.contains(id) =>
      sender() ! TaskRejected(s"Duplicated #$id")
      log.warning(s"Tried to duplicate task #$id")

    case AddTask(taskDefinition) =>
      import taskDefinition.id
      val actor = context.actorOf(Task.props(taskDefinition), s"Task-$id")
      tasks = tasks + (id -> TaskHolder(taskDefinition, actor))
      sender() ! TaskAccepted
      log.info(s"Task added: $taskDefinition")

    case RemoveTask(id) =>
      tasks.get(id).fold {
        sender() ! TaskNotFound
      } {
        case TaskHolder(_, actorRef) =>
          context.stop(actorRef)
          tasks = tasks - id
          log.info(s"Task removed #$id")
          sender() ! TaskRemoved
      }

    case GetTasks =>
      sender() ! tasks.values.map(_.definition)

    case GetTask(id) =>
      tasks.get(id).fold {
        sender() ! TaskNotFound
      } { holder =>
        sender() ! holder
      }

    case GetTaskNotIn(set) =>
      sender() ! tasks.collectFirst {
        case (id, task) if !set.contains(id) => task.definition
      }

    case PointsUpdated(taskId, points) =>
      tasks.get(taskId).foreach {
        case holder@TaskHolder(definition, _) =>
          log.debug(s"Updating task #$taskId points to $points")
          tasks = tasks + (taskId -> holder.copy(definition.copy(points = points)))
      }
  }
}
