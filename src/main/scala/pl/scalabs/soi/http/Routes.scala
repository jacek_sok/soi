package pl.scalabs.soi.http

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.pattern.ask
import akka.util.Timeout
import pl.scalabs.soi.Context.ConfigExt
import pl.scalabs.soi._

import scala.concurrent.Future

class Routes(context: Context) extends JsonSupport {
  import context._
  import actorSystem.dispatcher

  implicit val timeout = Timeout(config.getFiniteDuration("soi.ask-timeout"))

  private def customStatus(statusCode: StatusCode, reason: String) =
    StatusCodes.custom(statusCode.intValue, reason)

  val routing: Route =
    pathPrefix("task") {
      put {
        entity(as[TaskDefinition]) { task =>
          complete {
            (taskManager ? AddTask(task)) map {
              case TaskAccepted => Created
              case TaskRejected(reason) => customStatus(BadRequest, reason)
            }
          }
        }
      } ~
      delete {
        path(IntNumber) { id =>
          complete {
            (taskManager ? RemoveTask(id)) map {
              case TaskRemoved => Accepted
              case TaskNotFound => NotFound
            }
          }
        }
      } ~
      get {
        path(IntNumber) { id =>
          complete {
            (taskManager ? GetTask(id)) map {
              case TaskHolder(definition, _) => definition:ToResponseMarshallable
              case TaskNotFound => NotFound:ToResponseMarshallable
            }
          }
        } ~
        path("random") {
          parameter('user) { user =>
            complete {
              (userManager ? GetUser(user)) flatMap {
                case userActor: ActorRef => userActor ? PickRandomTask
                case other => Future.successful(other)
              } map {
                case Some(definition: TaskDefinition) => TaskDetail.fromDefinition(definition): ToResponseMarshallable
                case None => customStatus(NoContent, "No task available"): ToResponseMarshallable
                case TaskNotFound => customStatus(NotFound, "Task not found"): ToResponseMarshallable
                case UserDoesNotExists => customStatus(NotFound, "User doesn't exists"): ToResponseMarshallable
              }
            }
          }
        }
      }
    } ~
    pathPrefix("user" / Segment) { user =>
      put {
        complete {
          (userManager ? CreateUser(user)) map {
            case UserAlreadyExists => customStatus(Conflict, s"User already exists: $user")
            case _: ActorRef => Created
          }
        }
      } ~
      delete {
        complete {
          (userManager ? RemoveUser(user)) map {
            case UserDoesNotExists => NotFound
            case UserRemoved => Accepted
          }
        }
      } ~
      post {
        entity(as[PostAnswer]) { answer =>
          complete {
            (userManager ? GetUser(user)) flatMap {
              case userActor: ActorRef => userActor ? answer
              case other => Future.successful(other)
            } map {
              case result: AnswerResult =>
                val marshallable: ToResponseMarshallable = result match {
                  case r: CorrectAnswer => r
                  case r: IncorrectAnswer => r
                }
                marshallable
              case other =>
                val marshallable: ToResponseMarshallable = other match {
                  case TaskTimeOut => customStatus(GatewayTimeout, "Internal Timeout")
                  case TaskAlreadyTried => customStatus(Gone, "Task already tried")
                  case TaskNotFound => customStatus(NotFound, "Task not found")
                  case UserDoesNotExists => customStatus(NotFound, "User doesn't exists")
                }
                marshallable
            }
          }
        }
      }
    } ~
    get {
      path("ranking") {
        complete {
          (ranking ? GetRanking).mapTo[RankingList]
        }
      }
    }
}
