package pl.scalabs.soi.http

import pl.scalabs.soi.TaskDefinition

object TaskDetail {
  def fromDefinition(definition: TaskDefinition): TaskDetail = {
    import definition._
    TaskDetail(id, question, options, points)
  }
}

case class TaskDetail(id: Int, question: String, options: List[String], points: Double)
