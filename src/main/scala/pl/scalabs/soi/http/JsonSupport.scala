package pl.scalabs.soi.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import pl.scalabs.soi._
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val UserScoreFormat: RootJsonFormat[UserScore] = jsonFormat2(UserScore)
  implicit val TaskDefinitionFormat: RootJsonFormat[TaskDefinition] = jsonFormat5(TaskDefinition)
  implicit val PostAnswerFormat: RootJsonFormat[PostAnswer] = jsonFormat2(PostAnswer)
  implicit val CorrectAnswerFormat: RootJsonFormat[CorrectAnswer] = jsonFormat2(CorrectAnswer)
  implicit val IncorrectAnswerFormat: RootJsonFormat[IncorrectAnswer] = jsonFormat3(IncorrectAnswer)
  implicit val TaskDetailFormat: RootJsonFormat[TaskDetail] = jsonFormat4(TaskDetail.apply)
}
