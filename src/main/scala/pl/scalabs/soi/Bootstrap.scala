package pl.scalabs.soi

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import pl.scalabs.soi.http.Routes

object Bootstrap extends App {
  implicit val actorSystem = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val config = ConfigFactory.load()

  val ctx = Context.default()
  val routes = new Routes(ctx).routing

  import actorSystem.dispatcher

  Http()
    .bindAndHandle(routes, "0.0.0.0", 8080)
    .foreach { binding =>
      Runtime.getRuntime.addShutdownHook(shutdownHook(binding))
    }

  private def shutdownHook(binding: ServerBinding) =
    new Thread(() => binding.unbind())
}
