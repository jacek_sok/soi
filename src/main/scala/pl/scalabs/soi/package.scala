package pl.scalabs

import akka.actor.ActorRef

package object soi {
  case class TaskDefinition(id: Int, points: Double, question: String, options: List[String], answer: Set[Int])
  case class UserScore(user: String, points: Double)

  case class TryAnswer(answers: Set[Int])
  trait AnswerResult
  case class CorrectAnswer(taskId: Int, points: Double) extends AnswerResult
  case class IncorrectAnswer(taskId: Int, selected: Set[Int], correct: Set[Int]) extends AnswerResult

  case object GetRanking
  case class RemoveFromRanking(user: String)

  case class PostAnswer(taskId: Int, options: Set[Int])
  case object TaskAlreadyTried
  case object TaskTimeOut
  case class Check(taskId: Int)
  case object TaskNotTried
  case object TaskInProgress
  case object PickRandomTask
  case class PointsUpdated(taskId: Int, points: Double)

  case class AddTask(taskDefinition: TaskDefinition)
  case class RemoveTask(taskId: Int)
  case object GetTasks
  case class GetTask(taskId: Int)
  case class GetTaskNotIn(tasks: Set[Int])
  case class TaskHolder(definition: TaskDefinition, actorRef: ActorRef)
  trait TaskResponse
  case class TaskRejected(reason: String) extends TaskResponse
  case object TaskAccepted extends TaskResponse
  case object TaskNotFound extends TaskResponse
  case object TaskRemoved extends TaskResponse

  case class CreateUser(name: String)
  case class RemoveUser(name: String)
  case class GetUser(name: String)
  case object UserRemoved
  case object UserAlreadyExists
  case object UserDoesNotExists

  type RankingList = List[Seq[UserScore]]
}
